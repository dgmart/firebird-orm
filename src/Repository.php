<?php

namespace Hypersoft\Firebird;

use Hypersoft\Firebird\Query\Select;

class Repository
{
    /**
     * Nome da tabela que este repositorio gerencia
     * 
     * @var string
     */
    protected $table;

    /**
     * ORM para abstracao de dados
     * 
     * @var ORM
     */
    protected $orm;

    public function __construct($tableName, ORM $orm)
    {
        $this->table = $tableName;
        $this->orm = $orm;
    }

    /**
     * Retorna um registro vazio
     */
    public function create()
    {
        return $this->orm->create($this->table);
    }

    public function find($id)
    {
        return $this->orm->find($this->table, $id);
    }

    public function first($condition = [])
    {
        return $this->orm->first($this->table, $condition);
    }

    public function fetchColumn($column, $condition = [])
    {
        return $this->orm->fetchColumn($this->table, $column, $condition);
    }

    public function all($condition = [])
    {
        return $this->orm->all($this->table, $condition);
    }

    public function select()
    {
        return $this->orm->select($this->table);
    }

    public function insert($info)
    {
        return $this->orm->insert($this->table, $info);
    }

    public function lastInsertId()
    {
        throw new \Exception('Não implementado');
    }

    public function update($info)
    {
        return $this->orm->update($this->table, $info);
    }

    public function delete($id)
    {
        return $this->orm->delete($this->table, $id);
    }
}
