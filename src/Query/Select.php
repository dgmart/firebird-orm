<?php

namespace Hypersoft\Firebird\Query;

use Hypersoft\Firebird\Connection;

class Select implements \Iterator
{
    /* SQL Methods
     ------------------------------------ */

    private $conn;
    private $table;
    private $pjoin;
    private $pwhere;
    private $porder;
    private $plimit;

    public function __construct(Connection $conn, $table)
    {
        $this->conn = $conn;
        $this->table = $table;
    }

    public function where($condition, $value = null)
    {
        if (!$this->pwhere) {
            $this->pwhere = new Where();
        }
        $this->pwhere->where($condition, $value);

        return $this;
    }

    public function andWhere($condition, $value = null)
    {
        $this->where($condition, $value);

        return $this;
    }

    public function orWhere($condition, $value = null)
    {
        if (!$this->pwhere) {
            $this->pwhere = new Where();
        }
        $this->pwhere->orWhere($condition, $value);

        return $this;
    }

    public function innerJoin($table, $foreign_key, $local_key)
    {
        if (!$this->pjoin) {
            $this->pjoin = new Join();
        }
        $this->pjoin->join($table, $foreign_key, $local_key, Join::INNER);

        return $this;
    }

    public function leftJoin($table, $foreign_key, $local_key)
    {
        if (!$this->pjoin) {
            $this->pjoin = new Join();
        }
        $this->pjoin->join($table, $foreign_key, $local_key, Join::LEFT);

        return $this;
    }

    public function order($field, $order = OrderBy::ASC)
    {
        if (!$this->porder) {
            $this->porder = new OrderBy();
        }
        $this->porder->order($field, $order);

        return $this;
    }

    public function limit($limit)
    {
        if (!$this->plimit) {
            $this->plimit = new Limit();
        }
        $this->plimit->limit($limit);

        return $this;
    }

    public function offset($offset)
    {
        if (!$this->plimit) {
            $this->plimit = new Limit();
        }
        $this->plimit->offset($offset);

        return $this;
    }

    /* Iterator Methods
     -------------------------------------------- */

    private $pcurrent;
    private $pkey;
    private $pstm;

    public function current ()
    {
        return $this->pcurrent;
    }

    public function key ()
    {
        return $this->pkey;
    }

    public function next ()
    {
        $this->pcurrent = $this->pstm->fetch();
        $this->pkey++;
    }

    public function rewind ()
    {
        $sql = 'select * from ' . $this->table . ' ';
        $params = [];

        if ($this->pjoin) {
            $sql .= $this->pjoin->commit();
        }

        if ($this->pwhere) {
            $t = $this->pwhere->commit();
            $sql .= $t[0];
            $params = $t[1];
        }

        if ($this->porder) {
            $sql .= $this->porder->commit();
        }

        if ($this->plimit) {
            $sql .= $this->plimit->commit();
        }

        $this->pstm = $this->conn->query($sql, $params);
        $this->pkey = 0;
        $this->pcurrent = $this->pstm->fetch();
    }

    public function valid ()
    {
        return false != $this->pcurrent;
    }
}
