<?php

namespace Hypersoft\Firebird\Query;

class Where
{
    private $params;

    public function __construct()
    {
        $this->params = [];
    }

    public function validateCondition($condition)
    {
        $condition = strtolower(preg_replace('/\s+/', ' ', trim($condition)));
        if (!preg_match('/(\S+)\s?(is (not )*null|like|>=|<=|>|<|=)$/', $condition, $m)) {
            throw new \Exception('Operador inv�lido');
        }

        return ['field' => $m[1], 'operator' => $m[2]];
    }

    public function where($field, $value = null)
    {
        if ( ! ($field instanceof Where)) {
            $c = $this->validateCondition($field);
            if ('like' == $c['operator']) {
                $field = 'lower(' . $c['field'] . ') like';
                $value = strtolower(preg_replace('/\s+/', '%', trim($value)));
            }
        }
        $this->params[] = ['and', $field, $value];

        return $this;
    }

    public function orWhere($field, $value = null)
    {
        if ( ! ($field instanceof Where)) {
            $c = $this->validateCondition($field);
            if ('like' == $c['operator']) {
                $field = 'lower(' . $c['field'] . ') like';
                $value = strtolower(preg_replace('/\s+/', '%', trim($value)));
            }
        }
        $this->params[] = ['or', $field, $value];

        return $this;
    }

    public function commit()
    {
        $sql = '';
        $values = [];
        foreach ($this->params as $param) {
            if ($sql) {
                $sql .= ' ' . $param[0] . ' '; // AND | OR
            }
            if ($param[1] instanceof Where) {
                $t = $param[1]->commit();
                $sql .= '(' . ltrim($t[0], 'where ') . ')';
                $values = array_merge($values, $t[1]);
            } else {
                $sql .= $param[1] . ' ?'; // $param[1] --> "campo ="
                $values[] = $param[2];
            }
        }
        $sql = 'where ' . $sql . ' ';

        return [$sql, $values];
    }
}
