<?php

namespace Hypersoft\Firebird\Query;

class Join
{
    const LEFT = 'left';
    const INNER = 'inner';
}