<?php

namespace Hypersoft\Firebird\Query;

class OrderBy
{
    const ASC = 'asc';
    const DESC = 'desc';
    private $params;

    public function __construct()
    {
        $this->params = [];
    }

    public function order($field, $order = self::ASC)
    {
        if ($order != self::ASC && $order != self::DESC) {
            throw new \Exception('Clausula ORDER BY inv�lida');
        }

        $this->params[] = [$field, $order];
    }

    public function commit()
    {
        $sql = '';
        foreach ($this->params as $param) {
            $sql .= $param[0] . ' ' . $param[1] . ', ';
        }

        return 'order by ' . rtrim($sql, ', ') . ' ';
    }
}
