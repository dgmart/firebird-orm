<?php

namespace Hypersoft\Firebird\Query;

class Limit
{
    private $params;

    public function __construct()
    {
        $this->params = ['limit' => 0, 'offset' => 0];
    }

    public function limit($limit = 0)
    {
        $this->params['limit'] = $limit;
    }

    public function offset($offset)
    {
        $this->params['offset'] = $offset;
    }

    public function commit()
    {
        $sql = '';
        if ($this->params['limit']) {
            if ($this->params['offset']) {
                $sql = 'rows ' . $this->params['offset'] . ' to ' . ($this->params['offset'] + $this->params['limit']);
            } else {
                $sql = 'rows ' . $this->params['limit'];
            }
        }
        
        return $sql;
    }
}
