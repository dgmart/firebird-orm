<?php

namespace Hypersoft\Firebird;

use Psr\Log\LoggerInterface;

/**
 * Abstrai metodos de manuseio de banco de dados
 */
class Connection
{
    /**
     * PDO Connection
     *
     * @var PDO
     */
    private $conn;

    /**
     * Logger handler
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Abre a conexao com o banco de dados inicializa dependencias
     *
     * @param mixed[] parametros de conexao ao banco
     * @param LoggerInterface logger handler
     */
    public function __construct(array $params = [], LoggerInterface $logger = null)
    {
        $params = array_merge([
            'path' => 'DADOS.FDB',
            'host' => 'localhost',
            'user' => 'SYSDBA',
            'pass' => 'masterkey',
        ], $params);

        $this->conn = new \PDO(
            'firebird:dbname=' . $params['host'] . ':' . $params['path'],
            $params['user'],
            $params['pass'],
            [
                \PDO::ATTR_CASE => \PDO::CASE_LOWER,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
            ]
        );

        $this->logger = $logger;
    }

    /**
     * Permite alterar o logger informado no construtor
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * Confere se a conexao corrente esta em meio a alguma transacao
     *
     * @return boolean
     */
    public function inTransaction()
    {
        return $this->conn->inTransaction();
    }

    /**
     * Inicializa uma transacao
     *
     * @return boolean
     */
    public function beginTransaction()
    {
        if (!$this->inTransaction()) {
            $this->conn->setAttribute(\PDO::ATTR_AUTOCOMMIT, false);

            return $this->conn->beginTransaction();
        }

        return true;
    }

    /**
     * Persiste todas as alteracoes efetuadas durante a transacao
     *
     * @return boolean
     */
    public function commit()
    {
        if ($this->inTransaction()) {
            $this->conn->setAttribute(\PDO::ATTR_AUTOCOMMIT, false);

            return $this->conn->commit();
        }

        return true;
    }

    /**
     * Descarta todas as transacoes efetuadas durante a transacao
     *
     * @return boolean
     */
    public function rollBack()
    {
        if ($this->inTransaction()) {
            $this->conn->setAttribute(\PDO::ATTR_AUTOCOMMIT, false);

            return $this->conn->rollBack();
        }

        return true;
    }

    /**
     * Executa uma instrucao sql sem parametros
     *
     * @param string
     *
     * @return boolean
     */
    public function exec($sql)
    {
        if ($this->logger) {
            $this->logger->debug($sql);
        }

        return $this->conn->exec($sql);
    }

    /**
     * Execute uma consulta no banco com passagem de parametros via Statement
     *
     * @param string
     * @param mixed[] parametros para consulta, serao avaliados no molde WHERE <param1> AND <param2> ...
     *
     * @return PDOStatement
     */
    public function query($sql, $params = [])
    {
        if ($this->logger) {
            $this->logger->debug($sql, $params);
        }

        $stm = $this->conn->prepare($sql);
        $stm->execute($params);

        return $stm;
    }

    /**
     * Prepara uma consulta
     *
     * @param string
     *
     * @return PDOStatement
     */
    public function prepare($sql)
    {
        if ($this->logger) {
            $this->logger->debug($sql);
        }

        return $this->conn->prepare($sql);
    }

    /**
     * Retorna o primeiro registro da consulta
     *
     * @param string
     * @param mixed[] parametros para consulta, serao avaliados no molde WHERE <param1> AND <param2> ...
     *
     * @return mixed[]
     */
    public function first($sql, $condition = [])
    {
        $stm = $this->query($sql, $condition);

        return $stm->fetch();
    }

    /**
     * Retorna todos os itens da consulta
     *
     * @param string
     * @param mixed[] parametros para consulta, serao avaliados no molde WHERE <param1> AND <param2> ...
     *
     * @return mixed[]
     */
    public function fetchAll($sql, $params = [])
    {
        $stm = $this->query($sql, $params);
        $items = [];
        while (false != ($item = $stm->fetch())) {
            $items[] = $item;
        }

        return $items;
    }

    /**
     * Extrai a informacao de apenas uma coluna do primeiro registro da consulta
     *
     * @param string
     * @param mixed[] parametros para consulta, serao avaliados no molde WHERE <param1> AND <param2> ...
     * @param integer posicao da coluna que deseja retornar
     *
     * @return mixed
     */
    public function fetchColumn($sql, $params = [], $offset = 0)
    {
        $stm = $this->query($sql, $params);

        return $stm->fetchColumn($offset);
    }

    /**
     * Insere um registro na tabela indicada
     *
     * @param string nome da tabela a ser populada
     * @param mixed[] informacoes a serem inseridas
     *
     * @return PDOStatement
     */
    public function insert($table, $info)
    {
        $fields = '';
        $params = '';
        foreach ($info as $field => $value) {
            $fields .= $field . ',';
            $params .= '?,';
            $values[] = $value;
        }
        $sql = 'insert into ' . $table . '(' . rtrim($fields, ',') . ') values (' . rtrim($params, ',') . ')';

        return $this->query($sql, $values);
    }

    /**
     * Atualiza registros no banco de dados
     *
     * @param string nome da tabela a ser atualizada
     * @param mixed[] informacoes a serem atualizadas
     * @param mixed[] parametros para consulta, serao avaliados no molde WHERE <param1> AND <param2> ...
     *
     * @return PDOStatemente
     */
    public function update($table, $info, $condition)
    {
        $sql = 'update ' . $table . ' set ';
        foreach ($info as $field => $value) {
            $sql .= $field . '=?,';
            $values[] = $value;
        }
        $sql = rtrim($sql, ',') . ' where ';
        foreach ($condition as $field => $value) {
            $sql .= $field . '=? and ';
            $values[] = $value;
        }
        $sql = rtrim($sql, 'and ');

        return $this->query($sql, $values);
    }

    /**
     * Remove itens da tabela
     *
     * @param string nome da tabela
     * @param mixed[] parametros para consulta, serao avaliados no molde WHERE <param1> AND <param2> ...
     *
     * @return PDOStatement
     */
    public function delete($table, $condition)
    {
        $sql = 'delete from ' . $table . ' where ';
        foreach ($condition as $field => $value) {
            $sql .= $field . '=? and ';
            $values[] = $value;
        }
        $sql = rtrim($sql, 'and ');

        return $this->query($sql, $values);
    }
}
