<?php

namespace Hypersoft\Firebird;

use Psr\Log\LoggerInterface;
use Hypersoft\Firebird\Query\Select;
use Psr\SimpleCache\CacheInterface;

class ORM
{
    /**
     * Conexao com a base de dados
     *
     * @var Connection
     */
    private $conn;
    private $schema;
    private $cache;

    public function __construct(array $params = [], LoggerInterface $logger = null, CacheInterface $cache = null)
    {
        $this->conn = new Connection($params, $logger);
        $this->cache = $cache;
    }

    public function getConnection()
    {
        return $this->conn;
    }

    public function getRepository($table)
    {
        return new Repository($table, $this);
    }

    public function getSchema()
    {
        if (!$this->schema) {
            $this->schema = new Schema($this->conn);
        }

        return $this->schema;
    }

    public function beginTransaction()
    {
        if (!$this->conn->inTransaction()) {
            return $this->conn->beginTransaction();
        }

        return true;
    }

    public function commit()
    {
        if ($this->conn->inTransaction()) {
            return $this->conn->commit();
        }

        return true;
    }

    public function rollBack()
    {
        if ($this->conn->inTransaction()) {
            return $this->conn->rollBack();
        }

        return true;
    }

    public function create($table)
    {
        return array_fill_keys(
            $this->getSchema()->getTableColumns($table),
            null);
    }

    public function genId($generator, $step = 1)
    {
        $stm = $this->conn->query('select gen_id(' . $generator . ', ' . $step . ') as id from rdb$database');
        $item = $stm->fetch();

        return $item['id'];
    }

    public function select($table)
    {
        return new Select($this->conn, $table);
    }

    public function first($table, $condition = [])
    {
        $qry = $this->select($table);
        $qry->limit(1);

        foreach ($condition as $field => $value) {
            $qry->where($field . ' =', $value);
        }

        $qry->rewind();
        if ($qry->valid()) {
            return $qry->current();
        }

        return null;
    }

    public function find($table, $id)
    {
        // if ($this->cache) {
        //     $idCache = implode('.', !is_array($id) ? [$id] : $id);
        //     if (null !== ($item = $this->cache->get($table . '.' . $idCache))) {
        //         return $item;
        //     }
        // }

        $pks = $this->getSchema()->getPrimaryKey($table);
        if (!is_array($id)) {
            $id = [$pks[0] => $id];
        }

        $id = array_change_key_case($id);
        foreach ($pks as $pk) {
            if (!isset($id[$pk])) {
                throw new \Exception('Informar todas as chaves para o registro desejado');
            }
        }

        $item = $this->first($table, $id);
        // if ($this->cache && $item) {
        //     $this->cache->set($idCache, $item);
        // }

        return $item;
    }

    public function fetchColumn($table, $column, $condition = [])
    {
        $reg = $this->first($table, $condition);
        if (!$reg) {
            return null;
        }
        if (array_key_exists($column, $reg)) {
            return $reg[$column];
        }

        throw new \Exception('Coluna ' . $column . ' n�o definida');
    }

    public function all($table, $condition = [])
    {
        $qry = $this->select($table);
        foreach ($condition as $field => $value) {
            $qry->where($field . ' =', $value);
        }
        $list = [];
        foreach ($qry as $k => $v) {
            $list[$k] = $v;
        }

        return $list;
    }

    public function insert($table, $info)
    {
        $pks = $this->getSchema()->getPrimaryKey($table);
        $info = array_change_key_case($info);
        foreach ($pks as $pk) {
            if (isset($info[$pk]) && null === $info[$pk]) {
                unset($info[$pk]);
            }
        }

        return $this->conn->insert($table, $info);
    }

    public function update($table, $info)
    {
        $pks = $this->getSchema()->getPrimaryKey($table);
        $info = array_change_key_case($info);
        $condition = [];
        foreach ($pks as $pk) {
            if (!isset($info[$pk])) {
                throw new \Exception('Informar todas as chaves para o registro desejado');
            }
            $condition[$pk] = $info[$pk];
            unset($info[$pk]);
        }

        return $this->conn->update($table, $info, $condition);
    }

    public function delete($table, $id)
    {
        $pks = $this->getSchema()->getPrimaryKey($table);
        if (!is_array($id)) {
            if (1 != count($pks)) {
                throw new \Exception('Informar todas as chaves para o registro desejado');
            }

            return $this->conn->delete($table, [$pks[0] => $id]);
        } else {
            if (count($id) != count($pks)) {
                throw new \Exception('Informar todas as chaves para o registro desejado');
            }
            $id = array_change_key_case($id);
            foreach ($pks as $pk) {
                if (!isset($id[$pk])) {
                    throw new \Exception('Informar todas as chaves para o registro desejado');
                }
                $condition[$pk] = $id[$pk];
            }

            return $this->conn->delete($table, $condition);
        }
    }
}
