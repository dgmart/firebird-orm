<?php

namespace Hypersoft\Firebird;

class Schema
{
    private $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function getPrimaryKey($table)
    {
        static $cache = [];
        if (!isset($cache[$table])) {
            $sql = 'select trim(seg.rdb$field_name) as "field_name" ' .
                'from rdb$relation_constraints as rc ' .
                'inner join rdb$index_segments as seg on seg.rdb$index_name = rc.rdb$index_name ' .
                'where rc.rdb$constraint_type = \'PRIMARY KEY\' and upper(rc.rdb$relation_name) = ?';
            $stm = $this->conn->query($sql, [strtoupper($table)]);
            $fields = [];
            while (false != ($item = $stm->fetch())) {
                $fields[] = strtolower($item['field_name']);
            }
            $cache[$table] = $fields;
        }

        return $cache[$table];
    }

    public function getTableColumns($table)
    {
        static $cache = [];
        if (!isset($cache[$table])) {
            $sql = 'select rdb$field_name as "field_name" from rdb$relation_fields where rdb$relation_name = ?';
            $stm = $this->conn->query($sql, [strtoupper($table)]);
            $fields = [];
            while (false != ($item = $stm->fetch())) {
                $fields[] = strtolower(trim($item['field_name']));
            }
            $cache[$table] = $fields;
        }

        return $cache[$table];
    }
}
